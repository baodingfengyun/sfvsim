var currentSimulation;
var currentFrame = 0;
var isStopped = true;
var debug = false;
var playFrameTime = null;
var playFrame = null;
var framerate = 1000 / 60;

function tick() {
    if (isStopped) return;
    if (!nextFrame()) return;
    var currentFrameTime = new Date().getTime();
    var timeout;
    if (playFrameTime === null) {
        playFrameTime = currentFrameTime;
        playFrame = currentFrame;
        timeout = framerate;
    } else {
        timeout = framerate * (currentFrame - playFrame) - (currentFrameTime - playFrameTime);
    }
    setTimeout(tick, timeout);
}

function play() {
    if (currentFrame >= (currentSimulation.length - 1)) {
        currentFrame = 0;
    }
    isStopped = false;
    tick();
}

function pause() {
    isStopped = true;
    playFrame = null;
    playFrameTime = null;
}

function rewind() {
    pause();
    currentFrame = 0;
    drawScene(currentFrame);
}

function fastForward() {
    pause();
    currentFrame = currentSimulation.length - 1;
    drawScene(currentFrame);
}

function nextFrame() {
    if (currentFrame >= (currentSimulation.length - 1)) {
        pause();
        return false;
    }
    currentFrame++;
    drawScene(currentFrame);
    return true;
}

function previousFrame() {
    if (currentFrame < 1) {
        return;
    }
    currentFrame--;
    drawScene(currentFrame);
}

function drawBoxes(boxes, strokeColor, fillColor) {
    var drawn = {};
    return boxes.map(function (d) {
        return "<rect x='" + (d.x * 100) + "' y='" + (500 - (d.y + d.height) * 100) + "' width='" + (d.width * 100) + "' height='" + (d.height * 100) + "' stroke-width='2' fill-opacity='0.5' stroke-opacity='0.8' fill='" + fillColor + "' stroke='" + strokeColor + "' />";
    }).join("")+boxes.map(function (d) {
        var text = [];
        if (typeof d.effect_index !== "undefined") {
            // hitbox
            if (d.hit_type === 0) {
                text.push("🤜");
            }
            else if (d.hit_type === 1) {
                text.push(d.knockdown & 512 ? "/★" : "★");
            }
            else if (d.hit_type === 2) {
                text.push("🖐");
            }
            else if (d.hit_type === 5) {
                text.push("/★");
            }
            if (d.hit_level === 1) {
                text.push("▲");
            }
            else if (d.hit_level === 2) {
                text.push("▼");
            }
            if (d.flags & 2) {
                text.push("/C");
            }
            if (d.flags & 1) {
                text.push("/S");
            }
            if (d.flags & 4) {
                text.push("/A");
            }
        } else if (typeof d.hurt_type !== "undefined") {
            //
            if (d.vulnerability_flags & 4) {
                text.push("/A");
            }
            if (d.hurt_type & 4) {
                text.push("🖐");
            } else {
                if ((d.hurt_type & 1) === 0) {
                    text.push("/🤜");
                }
                if ((d.hurt_type & 2) === 0) {
                    text.push("/★");
                }
            }
        }
        var x = (d.x * 100);
        var y = (500 - (d.y + d.height) * 100);
        var cacheKey = Math.floor(x)+"|"+Math.floor(y);
        var shift = drawn[cacheKey] || 0;
        if (text) {
            drawn[cacheKey] = shift + text.length;
            text = text.map(function (t, i) {
                i += shift;
                var neg = "";
                if (t[0] === "/") {
                    t = t.substring(1);
                    neg = "<line x1='" + (x + 5 + i * 16) + "' x2='" + (x + 15 + i * 16) + "' y2='" + (y + 5) + "' y1='" + (y + 15) + "' stroke='black' stroke-width='1.5'></line>";
                }
                return "<circle r='8' cx='" + (x + 10 + i * 16) + "' cy='" + (y + 10) + "' fill='white' stroke='" + strokeColor + "'></circle>" +
                    "<text text-anchor='middle' dominant-baseline='central' x='" + (x + 10 + i * 16) + "' y='" + (y + 9) + "'>" + t + "</text>" + neg;
            }).join("");
        }
        return text;
    }).join("");
}

function drawCross(coord, color, width) {
    return "<line x1='" + (coord[0] * 100 - 5) + "' x2='" + (coord[0] * 100 + 5) + "' y1='" + (500 - coord[1] * 100) + "' y2='" + (500 - coord[1] * 100) + "' style='stroke:" + color + ";stroke-width:" + width + "'/>" +
        "<line x1='" + (coord[0] * 100) + "' x2='" + (coord[0] * 100) + "' y1='" + (500 - coord[1] * 100 - 5) + "' y2='" + (500 - coord[1] * 100 + 5) + "' style='stroke:" + color + ";stroke-width:" + width + "'/>";
}

function flt(x) {
    return Math.round(x * 10000) / 10000;
}

function displayStatus(status, state) {
    var playerStatus = [];
    if (state.status_air)
        playerStatus.push("AIR");
    if (state.status_crouched)
        playerStatus.push("CROUCHED");
    if (state.status_counter)
        playerStatus.push("COUNTER");
    var script = state.script.script;
    var char = state.script.temp_char ? state.script.temp_char : state.char;
    var stance = state.script.stance;
    var scriptName;
    scriptName = movesData[char].scriptIdx[stance][script];
    if (!scriptName && stance) {
        stance = 0;
        scriptName = movesData[char].scriptIdx[0][script];
    }
    if (!scriptName) {
        char = "CMN";
        scriptName = movesData[char].scriptIdx[0][script];
    }
    var invincibilities = {"STRIKE": 1, "PROJECTILE": 1, "THROW": 1, "AIR-ATTACK": 1};
    state.hurt.forEach(function (h) {
        if ((h.type & 4) === 4)
            delete invincibilities["THROW"];
        if ((h.type & 1) === 1) {
            delete invincibilities["STRIKE"];
            if (!h.air_inv)
                delete invincibilities["AIR-ATTACK"];
        }
        if ((h.type & 2) === 2)
            delete invincibilities["PROJECTILE"];
    });
    invincibilities = Object.keys(invincibilities);
    if (invincibilities.length === 4) {
        invincibilities = ["FULL"];
    }
    status.html(
        "Script: " + scriptName + " [" + char+"." + (stance ? "Alt." : "") + script + "]<br/>" +
        "Pos: " + flt(state.pos.coord[0])+", "+ flt(state.pos.coord[1])+", "+ flt(state.pos.speed[0])+", "+ flt(state.pos.speed[1]) + "<br/>" +
        "Script Time: " + state.script.time + "<br/>" +
        (state.stun !== null ? "Stun: " + state.stun + "<br/>" : "")+
        "V-Trigger: " + (state.vtrigger < 0 ? "&infin;" : state.vtrigger) + "<br/>" +
        "Status: " + playerStatus.join(", ") + "<br/>" +
        "Juggle: " + state.juggle_state + " ["+state.juggle+"]<br/>" +
        "Invincibility: " + invincibilities.sort().join(", ") + "<br/>" +
        "Combo: " + state.combo_counter + " [max: " + state.max_combo + "]"
    );
}
function initGrid() {
    var gridScene = "";
    for (var i = -750; i <= 750; i += 25) {
        gridScene += "<line x1='" + i + "' x2='" + i + "' y1='0' y2='500' "+(i % 100 === 0 ? "class='grid-strong-line'" : "")+"/>";
    }
    for (var i = 0; i < 500; i += 25) {
        gridScene += "<line y1='" + i + "' y2='" + i + "' x1='-750' x2='750' "+(i % 100 === 0 ? "class='grid-strong-line'" : "")+"/>";
    }
    $(".grid").html(gridScene);
}

function drawScene(index) {
    $("#frame-slider").slider("value", index);
    $("#frame-counter").html((index + 1) + " / " + currentSimulation.length);
    var state = currentSimulation[index];
    if (!state) {
        return;
    }
    if (debug) {
        $("#debug-p1").html(JSON.stringify(state[0], null, 4));
        $("#debug-p2").html(JSON.stringify(state[1], null, 4));
    }
    var recoveryNode = $(".status.recovery .recovery-value");
    recoveryNode.html(state[1].recovery);
    if (state[1].recovery > 0)
        recoveryNode.css("color", "green");
    else if (state[1].recovery < 0)
        recoveryNode.css("color", "red");
    else
        recoveryNode.css("color", "black");

    var rangeNode = $(".status.recovery .range-value");
    rangeNode.html(Math.round(Math.abs(state[0].pos.coord[0]-state[1].pos.coord[0])*1000.0)/1000);
    displayStatus($(".status.p1"), state[0]);
    displayStatus($(".status.p2"), state[1]);
    var all_hits = [].concat(
        state[0].hit,
        state[1].hit,
        [].concat.apply([], state[0].effects.map(function (e) {
            return e.hit;
        })),
        [].concat.apply([], state[1].effects.map(function (e) {
            return e.hit;
        }))
    ).filter(function (d) {
        return d.hit_type !== 4;
    });
    var all_effect_hurts = [].concat(
        [].concat.apply([], state[0].effects.map(function (e) {
            return e.hurt;
        })),
        [].concat.apply([], state[1].effects.map(function (e) {
            return e.hurt;
        }))
    );
    var scene =
        drawBoxes(state[0].hurt.filter(function(e) {return !e.armor_effect;}), "green", "lightgreen") +
        drawBoxes(state[1].hurt.filter(function(e) {return !e.armor_effect;}),  "blue", "lightblue") +
        drawBoxes(state[0].hurt.filter(function(e) {return e.armor_effect;}), "purple", "white") +
        drawBoxes(state[1].hurt.filter(function(e) {return e.armor_effect;}), "purple", "white") +
        drawBoxes(all_effect_hurts, "gray", "lightgray") +
        drawBoxes(state[0].phys, "orange", "orange") +
        drawBoxes(state[1].phys, "orange", "orange") +
        drawBoxes(all_hits, "red", "red") +
        drawCross(state[0].pos.coord, "green", 3) +
        drawCross(state[1].pos.coord, "blue", 3)
    ;
    $(".render").html(scene); // clear scene
}

function toggleDebug(isChecked) {
    debug = isChecked;
    if (debug) {
        $(".debug").show();
        drawScene(currentFrame);
    } else {
        $(".debug").hide();
    }
}
