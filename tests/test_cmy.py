from tests.util import SimulationRun


def test_cmy_jump():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-jump")
    run.execute("JUMP_F")


def test_cmy_8hk():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-8hk")
    run.execute("JUMP_F", 32)
    run.execute_and_check("8HK")


def test_cmy_8hp_corner():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-8hp-corner", x_p1=7, x_p2=7.5)
    run.execute("JUMP_V", 31)
    run.execute_and_check("8HP")


def test_cmy_5mp():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-5mp", 50)
    run.execute_and_check("5MP")


def test_cmy_5mp_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-5mp-guard", 50)
    run.p2.hit_effect = "GUARD"
    run.execute_and_check("5MP")


def test_cmy_5mp_corner():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-5mp-corner", x_p1=7, x_p2=7.5)
    run.execute_and_check("5MP")


def test_cmy_5hk():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-5hk", 40)
    run.execute_and_check("5HK")


def test_cmy_5hk_cc():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-5hk-cc", 40)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("5HK")


def test_cmy_2hk():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-2hk", 40)
    run.execute_and_check("2HK")


def test_cmy_2hk_kdbr():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-2hk-kdbr", 40)
    run.p2.rise = "BACK"
    run.execute_and_check("2HK")


def test_cmy_2hk_cc_kdbr():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-2hk-cc-kdbr", 40)
    run.p2.rise = "BACK"
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("2HK")


def test_cmy_2hk_kdr():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-2hk-kdr", 40)
    run.p2.rise = "QUICK"
    run.execute_and_check("2HK")


def test_cmy_2hk_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-2hk-guard", 40)
    run.p2.hit_effect = "GUARD"
    run.execute_and_check("2HK")


def test_cmy_2hk_cc():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-2hk-cc", 40)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("2HK")


def test_cmy_strike_l():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-strike-l", 20)
    run.execute("JUMP_F", 20)
    run.execute_and_check("STRIKE_L")


def test_cmy_strike_crouched():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-strike-l-crouched", 20)
    run.execute("JUMP_F", 15)
    run.execute_and_check("STRIKE_L", script_p2="CROUCH")


def test_cmy_strike_crouched_counter():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-strike-l-crouched-counter", 20)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute("JUMP_F", 15)
    run.execute_and_check("STRIKE_L", script_p2="CROUCH")


def test_cmy_strike_l_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-strike-l-guard", 20)
    run.p2.hit_effect = "GUARD"
    run.execute("JUMP_F", 20)
    run.execute_and_check("STRIKE_L")


def test_cmy_strike_m():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-strike-m", 20)
    run.execute("JUMP_F", 20)
    run.execute_and_check("STRIKE_M")


def test_cmy_strike_m_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-strike-m-guard", 20)
    run.p2.hit_effect = "GUARD"
    run.execute("JUMP_F", 20)
    run.execute_and_check("STRIKE_M")


def test_cmy_strike_h():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-strike-h", 20)
    run.execute("JUMP_F", 20)
    run.execute_and_check("STRIKE_H")


def test_cmy_spike_l_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spike-l-guard", 40)
    run.execute("STAND", 1)
    run.p2.hit_effect = "GUARD"
    run.execute_and_check("SPIKE_L")


def test_cmy_spike_l():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spike-l", 40)
    run.execute("STAND", 1)
    run.execute_and_check("SPIKE_L")


def test_cmy_spike_m():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spike-m", 40)
    run.execute("STAND", 1)
    run.execute_and_check("SPIKE_M")


def test_cmy_spike_h():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spike-h", 40)
    run.execute("STAND", 1)
    run.execute_and_check("SPIKE_H")


def test_cmy_spike_h_kdr():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spike-h-kdr", 40)
    run.p2.rise = "QUICK"
    run.execute("STAND", 1)
    run.execute_and_check("SPIKE_H")


def test_cmy_spike_h_kdbr():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spike-h-kdbr", 40)
    run.p2.rise = "BACK"
    run.execute("STAND", 1)
    run.execute_and_check("SPIKE_H")


def test_cmy_v_spike_l():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-spike-l", 40)
    run.p1.vtrigger = 2000
    run.execute("STAND", 1)
    run.execute_and_check("V_SPIKE_L")


def test_cmy_v_spike_h_kdbr():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-spike-h-kdbr", 40)
    run.p1.vtrigger = 2000
    run.p2.rise = "BACK"
    run.execute("STAND", 1)
    run.execute_and_check("V_SPIKE_H")


def test_cmy_v_spike_l_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-spike-l-guard", 40)
    run.p1.vtrigger = 2000
    run.p2.hit_effect = "GUARD"
    run.execute("STAND", 1)
    run.execute_and_check("V_SPIKE_L")


def test_cmy_v_strike_1hit():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-strike-1hit")
    run.p1.vtrigger = 2000
    run.execute("JUMP_F", 10)
    run.execute_and_check("V_STRIKE")


def test_cmy_v_strike_3hits():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-strike-3hits")
    run.p1.vtrigger = 2000
    run.execute("JUMP_F", 15)
    run.execute_and_check("V_STRIKE")


def test_cmy_v_strike_1hit_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-strike-1hit-guard")
    run.p1.vtrigger = 2000
    run.p2.hit_effect = "GUARD"
    run.execute("JUMP_F", 10)
    run.execute_and_check("V_STRIKE")


def test_cmy_v_strike_3hits_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-strike-3hits-guard")
    run.p1.vtrigger = 2000
    run.p2.hit_effect = "GUARD"
    run.execute("JUMP_F", 15)
    run.execute_and_check("V_STRIKE")


def test_cmy_v_strike_2hits_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-strike-2hits-guard", 18)
    run.p1.vtrigger = 2000
    run.p2.hit_effect = "GUARD"
    run.execute("JUMP_F", 33)
    run.execute_and_check("V_STRIKE")


def test_cmy_ex_strike():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-ex-strike")
    run.execute("JUMP_F", 15)
    run.execute_and_check("STRIKE_EX")


def test_cmy_ex_strike_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-ex-strike-guard")
    run.p2.hit_effect = "GUARD"
    run.execute("JUMP_F", 15)
    run.execute_and_check("STRIKE_EX")


def test_cmy_2hp_vtc():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-5hp-vtc", 30)
    run.execute("2HP", 24)
    run.execute_and_check("VTC_H")


def test_cmy_2hp_vtc_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-5hp-vtc-guard", 30)
    run.p2.hit_effect = "GUARD"
    run.execute("2HP", 24)
    run.execute_and_check("VTC_H")
    assert run.p1.vtrigger > 1900


def test_cmy_2mk_vtc_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-2mk-vtc", 30)
    run.execute("2MK", 22)
    run.execute_and_check("VTC_M")
    assert run.p1.vtrigger > 1900


def test_cmy_spiral_l():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spiral-l", 30)
    run.execute("STAND", 9)
    run.execute_and_check("ARROW_L")


def test_cmy_spiral_l_corner():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spiral-l-corner", x_p1=7, x_p2=7.5)
    run.execute("STAND", 2)
    run.execute_and_check("ARROW_L")


def test_cmy_spiral_h():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spiral-h", 30)
    run.execute("STAND", 9)
    run.execute_and_check("ARROW_H")


def test_cmy_spiral_m_corner():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spiral-m-corner-interrupted", x_p1=7, x_p2=7.5)
    run.execute("STAND", 2)
    run.execute_and_check("ARROW_M")


def test_cmy_spiral_h_corner():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spiral-h-corner", x_p1=7, x_p2=7.5)
    run.execute("STAND", 2)
    run.execute_and_check("ARROW_H")


def test_cmy_spiral_ex_corner():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spiral-ex-corner-interrupted", x_p1=7, x_p2=7.5)
    run.execute("STAND", 2)
    run.execute_and_check("ARROW_EX")


def test_cmy_spiral_h_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spiral-h-guard", 30)
    run.p2.hit_effect = "GUARD"
    run.execute("STAND", 9)
    run.execute_and_check("ARROW_H")


def test_cmy_dash():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-dash-interrupted", 1)
    run.execute("STAND", 1)
    run.execute("DASH")
    run.execute_and_check("STAND", 1)


def test_cmy_spiral_ex_1hit():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spiral-ex-1hit-interrupted")
    run.execute("STAND", 2)
    run.execute_and_check("ARROW_EX")


def test_cmy_spiral_ex():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-spiral-ex", 30)
    run.execute("STAND", 9)
    run.execute_and_check("ARROW_EX")


def test_cmy_v_spiral():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-spiral", 40)
    run.p1.vtrigger = 2000
    run.execute("STAND", 9)
    run.execute_and_check("V_ARROW")


def test_cmy_v_spiral_corner():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-spiral-corner-interrupted", x_p1=7, x_p2=7.5)
    run.p1.vtrigger = 2000
    run.execute("STAND", 2)
    run.execute_and_check("V_ARROW")


def test_cmy_v_spiral_direct():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-spiral-direct")
    run.p1.vtrigger = 2000
    run.execute("STAND", 2)
    run.execute_and_check("V_ARROW")


def test_cmy_v_spiral_3hit():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-spiral-3hit", -5)
    run.p1.vtrigger = 2000
    run.execute("STAND", 2)
    run.execute_and_check("V_ARROW")


def test_cmy_v_spiral_guard():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-spiral-guard", 40)
    run.p1.vtrigger = 2000
    run.p2.hit_effect = "GUARD"
    run.execute("STAND", 9)
    run.execute_and_check("V_ARROW")


def test_cmy_v_arrow_spike():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-v-arrow-spike")
    run.p1.vtrigger = 2000
    run.execute("STAND", 2)
    run.execute_and_check(["V_ARROW", "V_SPIKE_H"])


def test_cmy_throw_f():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-throw-f", 40)
    run.execute_and_check("THROW_F")


def test_cmy_hooligan_throw():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-hooligan-throw")
    run.execute("BACKWARD", 1)
    run.execute("STAND", 1)
    run.execute("HOOLIGAN_H", 26)
    run.execute_and_check("HOOLIGAN_SUKARI")


def test_cmy_hooligan_cross():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-hooligan-cross")
    run.execute("BACKWARD", 1)
    run.execute("STAND", 1)
    run.execute("HOOLIGAN_L", 26, move_name_p2="JUMP_F")
    run.execute_and_check("HOOLIGAN_SUKARI")


def test_cmy_hooligan_cross_kdbr():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-hooligan-cross-kdbr")
    run.p2.rise = "BACK"
    run.execute("BACKWARD", 1)
    run.execute("STAND", 1)
    run.execute("HOOLIGAN_L", 26, move_name_p2="JUMP_F")
    run.execute_and_check("HOOLIGAN_SUKARI")


def test_cmy_hooligan_whiff():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-hooligan-whiff")
    run.execute("BACKWARD", 1)
    run.execute("STAND", 1)
    run.execute("HOOLIGAN_L", 26)
    run.execute_and_check("HOOLIGAN_SUKARI")


def test_cmy_hooligan_ex():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-hooligan-ex")
    run.execute("STAND", 2)
    run.execute_and_check("HOOLIGAN_EX")


def test_cmy_hooligan_razor():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-hooligan-razor")
    run.execute("BACKWARD", 1)
    run.execute("STAND", 1)
    run.execute_and_check("HOOLIGAN_L")


def test_cmy_ca():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-ca")
    run.execute("STAND", 2)
    run.execute("FORWARD", 1)
    run.execute("STAND", 2)
    run.execute_and_check("CA_H", known_error_count=544)


def test_cmy_ca_corner():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-ca-corner", x_p1=5, x_p2=7.5)
    run.execute("STAND", 2)
    run.execute("FORWARD", 1)
    run.execute("STAND", 2)
    run.execute_and_check("CA_H", known_error_count=546)


def test_cmy_tc1_sa_spike_ex_corner():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-tc1-sa-spike-ex-corner", x_p1=7, x_p2=7.5)
    run.execute("4MP", 16)
    run.execute("TC1", 35)
    run.execute("ARROW_L", 55)
    run.execute_and_check("SPIKE_EX")


def test_cmy_tc1_sa_spike_whiff_corner():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-tc1-sa-spike-whiff-corner", x_p1=7, x_p2=7.5)
    run.execute("4MP", 16)
    run.execute("TC1", 35)
    run.execute("ARROW_L", 55)
    run.execute_and_check("SPIKE_L")


def test_cmy_4mp_aa_hk_whiff():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-4mp-aa-hk-whiff")
    run.execute("FORWARD", 10, move_name_p2="JUMP_F")
    run.execute("STAND", 20)
    run.execute("4MP", 34)
    run.execute_and_check("5HK")


def test_cmy_vskill_far():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-vskill-far")
    run.execute_and_check("V_SKILL")


def test_cmy_vskill_not_so_far():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-vskill-not-so-far", 33)
    run.execute_and_check("V_SKILL")


def test_cmy_vskill_close_enough():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-vskill-close-enough", 34)
    run.execute_and_check("V_SKILL")


def test_cmy_vskill_really_close():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-vskill-really-close", 45)
    run.execute_and_check("V_SKILL")


def test_cmy_air_to_air_8mp():
    run = SimulationRun("2.021", "CMY", "RYU", "cmy-air-to-air-8mp")
    run.execute("JUMP_F", 30, move_name_p2="JUMP_F")
    run.execute_and_check("8MP")


def test_cmy_spike_m_aa():
    run = SimulationRun("2.021", "CMY", "CMY", "cmy-spike-mk-aa")
    run.execute("STAND", 33, move_name_p2="JUMP_F")
    run.execute("STAND", 6, move_name_p2="8HP")
    run.execute_and_check("SPIKE_M")


def test_cmy_throw_tech():
    run = SimulationRun("3.060", "CMY", "RYU", "cmy-throw-tech", forward_p1=30, forward_p2=30)
    run.execute("FORWARD", 1, move_name_p2="THROW_F")
    run.execute_and_check("THROW_F")


def test_vt2_air():
    run = SimulationRun("3.060", "CMY", "RYU", "cmy-vt2")
    run.execute_and_check("VT2_START_AIR")
