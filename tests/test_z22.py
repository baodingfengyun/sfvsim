from tests.util import SimulationRun


def test_z22_vtrigger():
    run = SimulationRun("2.080", "Z22", "RYU", "z22-vtrigger")
    run.execute("VT_PSYCHO_CANON_LS", 100)
    run.execute_and_check("STAND", 200)


def test_z22_2hp_cc_vtrigger():
    run = SimulationRun("2.080", "Z22", "RYU", "z22-2hp-cc-vtrigger", 50)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute("2HP", 28)
    run.execute_and_check("VT_PSYCHO_CANON_LS", 250)
