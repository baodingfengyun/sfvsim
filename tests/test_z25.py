from tests.util import SimulationRun


def test_z25_vskill_old():
    run = SimulationRun("2.080", "Z25", "RYU", "z25-vskill-old", 40)
    run.execute_and_check("V_SKILL_G")


def test_z25_vskill_old_kdr():
    run = SimulationRun("2.080", "Z25", "RYU", "z25-vskill-old-kdr", 40)
    run.p2.rise = "QUICK"
    run.execute_and_check("V_SKILL_G")


def test_z25_vskill_young():
    run = SimulationRun("2.080", "Z25", "RYU", "z25-vskill-young", 40)
    run.execute("STAND", 2)
    run.execute_and_check(["SYUKUMYO_G", "V_SKILL_G"])

