from tests.util import SimulationRun


def test_ken_ca():
    run = SimulationRun("2.080", "KEN", "RYU", "ken-ca", 30)
    run.execute("STAND", 2)
    run.execute("FORWARD", 1)
    run.execute("STAND", 2)
    run.execute_and_check("CA_START", known_error_count=946)


def test_ken_throw():
    run = SimulationRun("2.080", "KEN", "RYU", "ken-throw", 50)
    run.execute_and_check("THROW_F")


def test_ken_throw_corner():
    run = SimulationRun("2.080", "KEN", "RYU", "ken-throw-corner", x_p1=7.0, x_p2=7.5)
    run.execute_and_check("THROW_F")


def test_ken_throw_f_ae():
    run = SimulationRun("3.060", "KEN", "RYU", "ken-throw-f-ae", x_p1=7.0, x_p2=7.5)
    run.execute_and_check("THROW_F")


def test_ken_tatsu_ex_whiff():
    run = SimulationRun("2.080", "KEN", "RYU", "ken-tatsu-ex-whiff")
    run.execute("STAND", 2)
    run.execute_and_check("TATSUMAKI_EX")


def test_ken_tatsu_ex_hit():
    run = SimulationRun("2.080", "KEN", "RYU", "ken-tatsu-ex-hit", 40)
    run.execute("STAND", 2)
    run.execute_and_check("TATSUMAKI_EX")


def test_ken_vt1_tatsu_h():
    run = SimulationRun("3.060", "KEN", "RYU", "ken-vt1-tatsu-h")
    run.execute("VT1_HEATUP")
    run.execute("JUMP_F", 21)
    run.execute_and_check("VT_AIR_TATSUMAKI_H")
