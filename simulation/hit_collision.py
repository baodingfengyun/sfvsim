from collections import defaultdict
from sfvfile import Hitbox, HitboxEffect, Hurtbox
from typing import Dict

from model import ZERO, FLT, TWO, Position, PlayerState, SFVData, is_resting_script, \
    Optional, StunForce, Set, List, Tuple, HitEffectSituation, HitEffectPosition, HitEffectFlags, is_falling, \
    is_landing_required, get_script_length, get_collision_priority, HitboxFlags
from .log import hit_logger


def apply_trade_system(data: SFVData, p1: PlayerState, p2: PlayerState, random_bool):
    """
    Apply the trading system as described in https://twitter.com/WydD/status/911725965453250560

    Will maybe remove one element from the hit_queue of one

    :param data: the SFVData to extract the move description when necessary
    :param p1: the player 1 state
    :param p2: the player 2 state
    :param random_bool: a random bool (usually equal to frame_counter % 2 == 1)
    """
    # make sure that we select the first hitbox
    if not p1.hit_queue:
        for p in p1.effects:
            if p.hit_queue:
                p1 = p
                break
    if not p2.hit_queue:
        for p in p2.effects:
            if p.hit_queue:
                p2 = p
                break
    # No trade to apply
    if not p1.hit_queue or not p2.hit_queue:
        return
    p1_hit = p1.hit_queue[0][0]
    p2_hit = p2.hit_queue[0][0]
    if p1_hit.hit_type == p2_hit.hit_type:
        if p1_hit.hit_type == 0:
            p1_move = data.get_current_move_description(p1)
            p2_move = data.get_current_move_description(p2)
            if p1_move is None or p2_move is None:
                # should not happen but it's a prevention
                hit_logger.warn("Unknown situation happened: priority system encountered a null move description")
                return
            # normal or special against each other
            if p1_move.category < 3 and p2_move.category < 3 and not p1.status_air and not p2.status_air:
                # normal vs normal
                if (p1_move.subcategories & 0x7) == (p2_move.subcategories & 0x7):
                    # pure trade
                    return
                # apply priority and force counter-hit
                if (p1_move.subcategories & 0x7) < (p2_move.subcategories & 0x7):
                    p1.hit_queue.pop(0)
                    p1.status_counter = True
                else:
                    p2.hit_queue.pop(0)
                    p2.status_counter = True
                return
            # Other situations are trade
            return
        if p1_hit.hit_type == 2:
            # Throw
            # at random invert p1 and p2
            if random_bool:
                p1, p2 = p2, p1
            # the attacker wins
            p2.hit_queue.pop(0)
        # Projectiles between themselves, let them trade
        return
    if p1_hit.hit_type > p2_hit.hit_type:
        # to avoid multiple condition, switch p1 & p2 to guarantee p1.type < p2.type
        p1, p2 = p2, p1
        p1_hit, p2_hit = p2_hit, p1_hit
    if p1_hit.hit_type == 0:
        # strike against throw or projectile
        if p2_hit.hit_type == 1:
            # standard trade against projectile
            return
        # against throws, the strike loses
        p1.hit_queue.pop(0)
        return
    if p1_hit.hit_type == 1:
        # projectiles against any kind of throws
        # throws loses
        p2.hit_queue.pop(0)
        return
    return


def projectile_trade(data: SFVData, p1: PlayerState, p2: PlayerState):
    """
    Perform the projectile trade step with absorbtion and reflect as well

    :param data: The SFV Data used for type5 hitboxes
    :param p1: The first player
    :param p2: The second player
    """
    all_p1_projectiles = [(h, p1) for h in p1.hit if h.hit_type == 1]
    all_p1_projectile_effects = [(h, e) for e in p1.effects for h in e.hit if h.hit_type == 1]
    all_p2_projectiles = [(h, p2) for h in p2.hit if h.hit_type == 1]
    all_p2_projectile_effects = [(h, e) for e in p2.effects for h in e.hit if h.hit_type == 1]

    consumed_boxes = defaultdict(lambda: set())
    # first pass, make the absorb and reflect work
    apply_type5(data, p1, all_p2_projectile_effects, consumed_boxes)
    apply_type5(data, p2, all_p1_projectile_effects, consumed_boxes)

    # second pass, make the projectile collide
    for ph1, ps1 in all_p1_projectiles + all_p1_projectile_effects:
        if ph1.group in consumed_boxes[ps1]:
            continue
        for ph2, ps2 in all_p2_projectiles + all_p2_projectile_effects:
            if ph2.group in consumed_boxes[ps2]:
                continue
            if ph1.does_collide(ph2):
                consumed_boxes[ps1].add(ph1.group)
                consumed_boxes[ps2].add(ph2.group)
                consume_hitbox(ps1, ph1)
                consume_hitbox(ps2, ph2)
                # now we apply hit effect on the target
                hit_freeze_1 = data.get_hitbox_effect(
                    ps1.current_char(), ph1.effect_index, HitEffectSituation.HIT,
                    HitEffectPosition.STAND, ps1.effects is None
                ).hit_freeze_defender
                hit_freeze_2 = data.get_hitbox_effect(
                    ps2.current_char(), ph2.effect_index, HitEffectSituation.HIT,
                    HitEffectPosition.STAND, ps1.effects is None
                ).hit_freeze_defender
                # each apply a freeze on each other
                ps1.stun = (ps1.stun or 0) + hit_freeze_2
                ps2.stun = (ps2.stun or 0) + hit_freeze_1
                # TODO add a freeze test (e.g. zgf 6HP vs ibk vskill)
                hit_logger.info(
                    "[%s][%s] - [%s][%s] Traded projectile groups %d - %d, freeze frames %d - %d",
                    ps1.char, ps1.script, ps2.char, ps2.script, ph1.group, ph2.group, hit_freeze_2, hit_freeze_1
                )
                break

    # final pass, remove all used boxes
    for ps, hitboxes in consumed_boxes.items():
        if not hitboxes:
            continue
        ps.hit = [h for h in ps.hit if h.group not in hitboxes]


def apply_type5(data: SFVData, p: PlayerState, opponent_projectile_effects: List[Tuple[Hitbox, PlayerState]], consumed_boxes: Dict[PlayerState, Set[int]]):
    """
    Apply the hitbox type 5 collision and effect

    :param data: The SFV Data
    :param p: The main player state to apply
    :param opponent_projectile_effects: The list of opponent hitbox+playerstate
    :param consumed_boxes: The dictionary of consumed hitboxes
    """
    # First: gather all the hitboxes
    all_type5 = [
        (h, p, data.get_hitbox_effect(p.current_char(), h.effect_index, HitEffectSituation.GUARD, HitEffectPosition.STAND))
        for h in p.hit if h.hit_type == 5
    ] + [
        (h, e, data.get_hitbox_effect(e.current_char(), h.effect_index, HitEffectSituation.GUARD, HitEffectPosition.STAND, True))
        for e in p.effects
        for h in e.hit if h.hit_type == 5
    ]
    """:type: list[Tuple[sfvfile_binding.Hitbox, PlayerState, sfvfile_binding.HitboxEffect]]"""

    # Now do the collision
    ignore_effects = set()
    for ph1, ps1, he in all_type5:
        if ph1.group in consumed_boxes[ps1]:
            continue
        for ph2, ps2 in opponent_projectile_effects:
            if ps2 in ignore_effects:
                continue
            if ph2.group in consumed_boxes[ps2]:
                continue
            if ph1.strength < ph2.strength:
                # we have to check if the hit_level is high enough to perform the collision as expected
                continue
            # collide
            if ph1.does_collide(ph2):
                # now we execute the effect
                # first the stun
                if he.hit_freeze_attacker > 0:
                    ps1.stun = he.hit_freeze_attacker
                ps2.stun = max(1, he.hit_freeze_defender)
                # absorb the box
                if he.knock_back > 0:
                    # reflect
                    # transfer the effect to the other player
                    ps1.opponent.opponent.effects.append(ps2)
                    ps2.opponent.opponent.effects.remove(ps2)

                    ps2.pos.speed[0] = ps2.pos.speed[0] * he.knock_back
                    ps2.pos.force[0] = ps2.pos.force[0] * he.knock_back

                    # mirror the position relative to the hitbox we just hit
                    new_pos = (ph2.x - ps2.pos.ref[0]) + ph2.x + ph2.width
                    ps2.pos.ref[0] = new_pos
                    ps2.pos.shift[0] = -ps2.pos.shift[0]
                    ps2.pos.coord[0] = ps2.pos.ref[0] + ps2.pos.shift[0]
                    ps2.pos.side = not ps2.pos.side

                    # change the opponent
                    ps2.opponent = ps2.opponent.opponent
                    ps2.hit_state = ps1.opponent.opponent.hit_state
                    consume_hitbox(ps1, ph1)
                    consumed_boxes[ps2].update({h.group for h in ps2.hit})
                    hit_logger.info(
                        "[%s][%s] Reflecting projectile %d using %d[knockback=%f, stun=%d]",
                        ps1.char, ps1.script, ph2.group, he.index, he.knock_back, he.hit_freeze_defender
                    )
                else:
                    # absorb the hit like a standard projectile vs projectile
                    consume_hitbox(ps1, ph1)
                    consume_hitbox(ps2, ph2)
                    consumed_boxes[ps1].add(ph1.group)
                    consumed_boxes[ps2].add(ph2.group)
                    hit_logger.info(
                        "[%s][%s] Absorbed projectile %d using %d[knockback=%f, stun=%d]",
                        ps1.char, ps1.script, ph2.group, he.index, he.knock_back, he.hit_freeze_defender
                    )
                break


def perform_hit_collision(p: PlayerState):
    """
    Apply hit collision based on p.hit hitboxes against the opponent hurtboxes while respecting box properties.

    Will apply do_box_collide on each collision (even if multiple hitboxes hit)

    :param p:
    :return:
    """
    for h in p.hit:
        if h.hit_type >= 4:
            continue
        if p.opponent.script.temp_char is not None and p.effects is not None:
            enqueue_box_collide(p, h, None)
            continue

        if h.hit_restriction & 4 and p.opponent.status_air:
            continue
        if h.hit_restriction & 2 and p.opponent.status_crouched:
            continue
        if h.hit_restriction & 1 and (not p.opponent.status_crouched and not p.opponent.status_air):
            continue

        is_cross_up = p.pos.side == p.opponent.pos.side
        if h.hit_restriction & 0x20 and not is_cross_up:
            # cant hit non-cross up
            continue
        if h.hit_restriction & 0x40 and is_cross_up:
            # cant hit cross up
            continue
        # okay... what's this?
        if h.flags & HitboxFlags.IGNORE:
            continue

        if p.effects is not None and p.opponent.effects:
            has_hit = None
            for eff in list(p.opponent.effects):
                has_hit = perform_hit_collision_with_hurtboxes(p, h, eff.hurt, lambda p, h, h2: enqueue_box_collide(p, h, h2, eff))
                if has_hit is not None:
                    break
            if has_hit is not None:
                if has_hit:
                    # this is a 255 box hit
                    break
                else:
                    # this is a normal box hit
                    continue

        if perform_hit_collision_with_hurtboxes(p, h, p.opponent.hurt, enqueue_box_collide):
            # this is a 255 box hit
            return


def perform_hit_collision_with_hurtboxes(p: PlayerState, h: Hitbox, hurtboxes: List[Hurtbox], action) -> Optional[bool]:
    """
    Perform the collision of a hitbox against hurtboxes. It the strike is performed, apply the action

    Will take into account hurtbox properties

    :param p: Player state
    :param h: Hitbox
    :param hurtboxes: Hurtboxes
    :param action: The action to apply
    :return: None if nothing has hit, True if something has hit and nothing else must hit on this frame,
     False if normal hit or armor
    """
    for h2 in sorted(hurtboxes, key=get_collision_priority):
        if h.hit_type == 0 and h2.hurt_type >= 4:
            continue
        if h.hit_type == 2 and h2.hurt_type < 4:
            continue
        if h.hit_type == 1 and (h2.hurt_type & 2) == 0:
            continue
        if (h2.vulnerability_flags & 4) and p.status_air:
            continue
        if h.does_collide(h2):
            if h2.armor_effect == 0:
                action(p, h, h2)
                if h.group != 255:
                    return True
                return False
            if armor_hit(p, h, h2):
                return False
    return None


def armor_hit(p: PlayerState, hit: Hitbox, hurt: Hurtbox):
    """
    Try to armor a hit with an armored hurtbox.

    :param p: the player state
    :param hit: the hitbox to armor
    :param hurt: the hurtbox
    :return: true if the hit has been absorbed
    """
    armor_group = hurt.armor_group
    if armor_group not in p.opponent.armor:
        p.opponent.armor[armor_group] = []
    armor = p.opponent.armor[armor_group]
    if len(armor) >= hurt.armor_count:
        # we have exceed the armor limit
        # we can safely ignore this armor hurtbox
        return False
    # freeze me because I've been armored for the amount of time required by the armor (if any)
    p.stun = (p.stun or 0) + hurt.armor_freeze
    # we have indeed armored the hit
    armor.append(hit)
    # do consume the hit nonetheless
    consume_hitbox(p, hit)
    return True


def consume_hitbox(p, hit: Hitbox):
    """
    Consume a hitbox in the script

    :param p: the player state
    :param hit: the hitbox to consume
    """
    if hit.group != 255 or p.effects is None:
        p.hit_state.used_hitboxes.add(hit.group)
    p.hit_state.hit_hitboxes.add(hit.group)


def enqueue_box_collide(p: PlayerState, h: Hitbox, hurt: Optional[Hurtbox], eff: Optional[PlayerState] = None):
    """
    Enqueue the box for actual collision.

    The box is put on the hit queue only if it's not already there.
    If the number of hits is greater than one, than the hitbox is applied more than once.

    :param p: the state of the player hitting
    :param h: the box
    :param hurt: the target hurtbox
    """
    # check if a similar hitbox is already in the queue
    if h.group != 255:
        for hb, hurt in p.hit_queue:
            if hb.group == h.group:
                consume_hitbox(p, h)
                return
    if eff is None:
        # append the hitbox to the queue
        for _ in range(max(1, h.number_of_hits)):
            p.hit_queue.append((h, hurt))
    else:
        p.opponent.effects.remove(eff)
    consume_hitbox(p, h)


def apply_hit_queue(data: SFVData, p: PlayerState, hitbox: Hitbox, hurtbox: Optional[Hurtbox]):
    """
    Actually execute the hit collision.

    :param data: the data
    :param p: the player state
    :param hitbox: the hitbox that has stroke
    :param hurtbox: the hurtbox that has been struck
    """
    effect_index = hitbox.effect_index
    juggle_increase = hitbox.juggle_increase
    side = FLT(1 if p.opponent.opponent.pos.coord[0] < p.opponent.pos.coord[0] else -1)
    if 220 <= p.opponent.script.script < 230:
        effect = data.get_hitbox_effect(p.current_char(), effect_index, HitEffectSituation.HIT, HitEffectPosition.OTG, p.effects is None)
        # only type 4 will hit otg
        if effect.type != 4:
            # ignore the box, i'm pretty sure this method will backfire at me some day
            p.hit_state.hit_hitboxes.remove(hitbox.group)
            return
        hit_effect = HitEffectSituation.HIT
        opponent_status = HitEffectPosition.OTG
    else:
        opponent_status = HitEffectPosition.AIR if p.opponent.status_air else (HitEffectPosition.CROUCH if p.opponent.status_crouched else HitEffectPosition.STAND)

        apply_hit_effect = p.opponent.hit_effect
        if apply_hit_effect == "GUARD2ND":
            apply_hit_effect = "HIT"
            p.opponent.hit_effect = "GUARD"
        # cant counterhit twice (multi counter not supported yet)
        if apply_hit_effect == "COUNTERHIT" and p.combo_counter > 0:
            apply_hit_effect = "HIT"
        if apply_hit_effect == "GUARD":
            if not (is_resting_script(p.opponent.char, p.opponent.script.script) or 50 <= p.opponent.script.script < 120):
                # Cant guard if not resting or already guarding
                apply_hit_effect = "HIT"
            elif opponent_status == HitEffectPosition.AIR:
                # Cant guard in the air
                apply_hit_effect = "HIT"
            elif hitbox.hit_type == 2:
                # Cant guard a throw
                apply_hit_effect = "HIT"

        if apply_hit_effect == "HIT" and p.opponent.status_counter and p.opponent.opponent.combo_counter == 0:
            apply_hit_effect = "COUNTERHIT"

        if apply_hit_effect == "HIT":
            hit_effect = HitEffectSituation.HIT
        elif apply_hit_effect == "COUNTERHIT":
            hit_effect = HitEffectSituation.COUNTERHIT
        elif apply_hit_effect == "GUARD":
            hit_effect = HitEffectSituation.GUARD
        else:
            hit_effect = HitEffectSituation.UNKNOWN

        effect = data.get_hitbox_effect(p.current_char(), effect_index, hit_effect, opponent_status, p.effects is None)
    recovery = effect.main_recovery_duration
    hit_animation = effect.index
    if not (effect.damage_type & HitEffectFlags.SPECIFIC_SCRIPT):
        if opponent_status == HitEffectPosition.AIR:
            hurt_type = 4
        elif opponent_status == HitEffectPosition.CROUCH:
            hurt_type = 3
        elif hurtbox is not None:
            hurt_type = hurtbox.hurt_level
        else:
            hurt_type = 0
        # force hurt type to be correct
        hit_range = hitbox.hit_range >> 4
        if effect.type == 1:
            if effect.damage_type & HitEffectFlags.CRUSH:
                hit_animation = 170 + hurt_type * 3 + hit_range
            else:
                hit_animation += 120 + hurt_type * 9 + hit_range * 3
        elif effect.type == 2:
            hit_animation += 70 + hurt_type * 9 + hit_range * 3
        elif effect.type == 3:
            hit_animation += 210
        elif effect.type == 4:
            hit_animation += 220
    if effect.damage_type & HitEffectFlags.HARD_KD:
        p.opponent.knockdown_type = "HARD"
    elif hitbox.flags & 16 or effect.damage_type & HitEffectFlags.NO_BACK_RECOVERY:
        p.opponent.knockdown_type = "STAND"
    else:
        p.opponent.knockdown_type = "NORMAL"
    if not(220 <= p.opponent.script.script < 230):
        p.opponent.queue = []
    target_script = data.get_current_script(p, hit_animation) if effect.type > 0 else None
    p.opponent.script.speed = None
    falling = (is_falling(target_script.flags) or is_landing_required(target_script.flags)) if target_script is not None else False
    if 1 <= effect.type < 3 and not falling and recovery > 0:
        vx = effect.knock_back * TWO / FLT(recovery)
        p.opponent.stun_force = StunForce()
        p.opponent.stun_force.speed[0] = side * vx
        p.opponent.stun_force.force[0] = - side * vx / FLT(recovery)
        p.opponent.stun_force.t = recovery
        frame_to_target = target_script.last_action_frame if target_script.last_action_frame > 0 else get_script_length(target_script)
        speed = int(0x10000*frame_to_target/recovery)
        p.opponent.script.speed = [speed, (0x10000 - speed*recovery % 0x10000) % 0x10000, recovery]
    elif effect.type == 3 or (effect.type == 1 and falling):
        vx = effect.knock_back / FLT(recovery)
        p.opponent.stun_force = StunForce()
        if effect.fall_speed > 0:
            y_recovery = FLT(recovery) / TWO
            vy = effect.fall_speed * TWO / y_recovery
            p.opponent.stun_force.force[1] += -FLT(vy) / y_recovery
        else:
            # If fall_speed is negative
            # Rules are not the same \o/
            vy = effect.fall_speed / FLT(recovery)
        p.opponent.stun_force.speed[0] += side * vx
        p.opponent.stun_force.speed[1] += vy
        p.opponent.stun_force.t = 10000
    elif effect.type == 4:
        if opponent_status == HitEffectPosition.OTG and p.opponent.stun_force:
            # OTG push back = slow down the current script
            p.opponent.stun_force.speed[0] /= FLT(5)
            p.opponent.stun_force.force[0] = ZERO
            p.opponent.stun_force.t = 10000
    elif effect.type == 0:
        pass
    if hitbox.hit_type != 2:
        p.stun = effect.hit_freeze_attacker
        p.opponent.stun = effect.hit_freeze_defender
    if 0 < effect.type and 0 < hit_animation:
        p.opponent.change_script(hit_animation, None)
        # reset vy ay
        p.opponent.pos.speed[1] = ZERO
        p.opponent.pos.force[1] = ZERO
        if effect.type == 1:
            if p.opponent.juggle_state == "JUGGLE":
                p.opponent.juggle_state = "RESET"
                p.opponent.juggle += juggle_increase
            elif opponent_status == "AIR":
                p.opponent.juggle_state = "RESET"
                p.opponent.juggle += juggle_increase
        elif effect.type == 2:
            p.opponent.juggle_state = "NONE"
            p.opponent.juggle = 0
        elif effect.type == 3:
            if p.opponent.juggle_state == "JUGGLE":
                p.opponent.juggle += juggle_increase
            else:
                p.opponent.juggle_state = "JUGGLE"
                p.opponent.juggle = effect.juggle_start
    hit_logger.info(
        "[%s][%s] Has hit opponent[%s, %s] with %d[%d, recovery=%d stun=%d animation=%d downtime=%d]",
        p.char, p.script, hit_effect, opponent_status, effect_index, effect.type, recovery, p.stun if p.stun is not None else -1,
        hit_animation,
        effect.knockdown_duration
    )
    p.opponent.downtime = effect.knockdown_duration
    if effect.damage > 0:
        if hit_effect != HitEffectSituation.GUARD:
            p.opponent.opponent.combo_counter += 1
        else:
            p.opponent.opponent.combo_counter = 0
    if effect.vtimer:
        p.opponent.opponent.vtrigger += effect.vtimer
    p.hit_state.has_hit += (-1 if hit_effect == HitEffectSituation.GUARD else 1)
    p.hit_state.hit_status = (2 if hit_effect == HitEffectSituation.GUARD else 1)

