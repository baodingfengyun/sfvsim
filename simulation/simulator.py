import gzip
import os
import pickle

import time

from zipfile36 import ZipFile

from model import *
from simulation.transitions import main_transition, final_transition, execute_opponent_transition
from .hit_collision import perform_hit_collision, apply_hit_queue, apply_trade_system, projectile_trade
from .box_management import compute_physical_box, filter_hitboxes
from .movement import compute_movement
from .physical_collision import apply_physical_collision, BOUND
from .log import *


def uasset_path(uasset_root, char, uasset_type, version, eff=""):
    return "%s/%s/BattleResource/%s/%s_%s%s.uasset" % (uasset_root, char, version, uasset_type, char, eff)


file_cache = dict()


def load_file(zf, clazz, path):
    #print("Loading file", path)
    if path not in file_cache:
        result = file_cache[path] = clazz(zf.open(path).read())
        return result
    return file_cache[path]


def load(version_txt, uasset_root="StreetFighterV/Content/Chara") -> SFVData:
    print("Loading data")

    with ZipFile(os.path.dirname(__file__) + "/../StreetFighterV.zip", "r") as zf:
        t = time.time()
        char_versions = {
            line[:3]: CharacterData(
                load_file(zf, BAC, uasset_path(uasset_root, line[:3], "BAC", line[4:])),
                load_file(zf, BAC, uasset_path(uasset_root, line[:3], "BAC", line[4:], "_eff")) if line[:3] != "CMN" else None,
                load_file(zf, BCH, uasset_path(uasset_root, line[:3], "BCH", line[4:])),
                load_file(zf, BCM, uasset_path(uasset_root, line[:3], "BCM", line[4:]))
            )
            for line in open(os.path.dirname(__file__) + "/../" +version_txt).read().strip().split("\n")
        }
        data = SFVData(char_versions)

    print("Data Loaded in", time.time() - t)
    return data


def load_latest(version_txt, uasset_root="StreetFighterV/Content/Chara") -> SFVData:
    print("Loading data")

    with ZipFile(os.path.dirname(__file__) + "/../StreetFighterV.zip", "r") as zf:
        t = time.time()
        char_versions = {
            line[:3]: CharacterData(
                load_file(zf, BAC, uasset_path(uasset_root, line[:3], "BAC", line[4:])),
                load_file(zf, BAC, uasset_path(uasset_root, line[:3], "BAC", line[4:], "_eff")) if line[:3] != "CMN" else None,
                load_file(zf, BCH, uasset_path(uasset_root, line[:3], "BCH", line[4:])),
                load_file(zf, BCM, uasset_path(uasset_root, line[:3], "BCM", line[4:]))
            )
            for line in open(os.path.dirname(__file__) + "/../" +version_txt).read().strip().split("\n")
        }
        data = SFVData(char_versions)

    print("Data Loaded in", time.time() - t)
    return data

def default_next():
    return [0], 1


# noinspection PyUnusedLocal
def default_can_transition(cancel_lists):
    return False


def remove_effects(cl, p, e):
    if cl is not None:
        return
    if e in p.effects:
        p.effects.remove(e)


def compute_auto_correct_side(p: PlayerState, p2_x: float):
    return p.pos.ref[0] < p2_x if p.pos.ref[0] != p2_x else None

class MoveNotAllowed(Exception):
    def __init__(self, data, p, move_name, y=None, height_restriction=None, vtrigger=None, stance=None):
        if height_restriction is not None:
            super().__init__("Cannot cancel into %s while player is in script %s on "
                             "frame %d because of a position restriction %f < %f" % (
                                 move_name,
                                 data.get_current_script(p).name, p.script.time, y,
                                 height_restriction))
        elif vtrigger is not None:
            super().__init__(
                "Cannot execute %s because it requires %d of v-trigger gauge and you have none" % (
                    move_name,
                    vtrigger
                ))
        elif stance is not None:
            super().__init__(
                "Cannot select this move %s because you are not in the right stance" % (
                    move_name
                ))
        else:
            super().__init__("Cannot cancel into %s while player is in script %s on frame %d" % (
                move_name,
                data.get_current_script(p).name,
                p.script.time))


class Simulator:
    def __init__(self, data_bin):
        self.data = load(data_bin)

    def step(self, p1: PlayerState, p2: PlayerState, get_next_p1=default_next, get_next_p2=default_next,
             can_transition_p1=default_can_transition, can_transition_p2=default_can_transition, frame_count=0):
        """
        Execute one frame of the simulation

        :param p1: the first player state
        :param p2: the second player state
        :param get_next_p1: function that returns a tuple (move, hold) that corresponds to the next move in queue for p1
        :param get_next_p2: same thing as get_next_p1 for p2
        :param can_transition_p1: returns true if p1 can transition to another move
        :param can_transition_p2: same thing as can_transition_p1 for p2
        :param frame_count: the frame count
        """
        # Transition step
        script_p1 = self.transition_step(p1, get_next_p1, can_transition_p1)
        script_p2 = self.transition_step(p2, get_next_p2, can_transition_p2)
        scripts_p1_eff = [(e, self.transition_step(e, default_next, lambda cl: remove_effects(cl, p1, e), False)) for e
                          in p1.effects]
        scripts_p2_eff = [(e, self.transition_step(e, default_next, lambda cl: remove_effects(cl, p2, e), False)) for e
                          in p2.effects]

        # Apply opponent transition if any
        script_p1, script_p2 = self.opponent_transition_step(p1, script_p1, script_p2)
        script_p2, script_p1 = self.opponent_transition_step(p2, script_p2, script_p1)

        # Move positions
        x1, y1, x2, y2 = p1.pos.coord[0], p1.pos.coord[1], p2.pos.coord[0], p2.pos.coord[1]
        xref1, xref2 = p1.pos.ref[0], p2.pos.ref[0]
        go_to_next_p1 = self.position_change_step(p1, script_p1, x2, y2, compute_auto_correct_side(p1, xref2))
        go_to_next_p2 = self.position_change_step(p2, script_p2, x1, y1, compute_auto_correct_side(p2, xref1))
        go_to_next_p1_eff = [(e, script, self.position_change_step(e, script, x2, y2, compute_auto_correct_side(e, xref2))) for e, script in scripts_p1_eff]
        go_to_next_p2_eff = [(e, script, self.position_change_step(e, script, x1, y1, compute_auto_correct_side(e, xref1))) for e, script in scripts_p2_eff]

        # Phy box collision
        self.physical_correction_step(p1, p2, script_p1, script_p2, x1 < x2)

        # Box computation
        self.compute_hit_hurt_step(p1, script_p1)
        self.compute_hit_hurt_step(p2, script_p2)
        for e, script in scripts_p1_eff:
            self.compute_hit_hurt_step(e, script)
        for e, script in scripts_p2_eff:
            self.compute_hit_hurt_step(e, script)

        # Projectile nullify
        projectile_trade(self.data, p1, p2)

        # Strike !
        self.hit_collision(p1, script_p1)
        self.hit_collision(p2, script_p2)
        for e, script, go_to_next_eff in go_to_next_p1_eff:
            self.hit_collision(e, script)
        for e, script, go_to_next_eff in go_to_next_p2_eff:
            self.hit_collision(e, script)

        # Apply hit effects on both player after trading
        apply_trade_system(self.data, p1, p2, frame_count % 2 == 1)
        self.apply_hit_queue(p1, script_p1)
        self.apply_hit_queue(p2, script_p2)
        for e, script, go_to_next_eff in go_to_next_p1_eff:
            self.apply_hit_queue(e, script)
        for e, script, go_to_next_eff in go_to_next_p2_eff:
            self.apply_hit_queue(e, script)

        # Final transition
        self.final_transition_step(p1, script_p1, go_to_next_p1)
        self.final_transition_step(p2, script_p2, go_to_next_p2)
        for e, script, go_to_next_eff in go_to_next_p1_eff:
            self.final_transition_step(e, script, go_to_next_eff)
        for e, script, go_to_next_eff in go_to_next_p2_eff:
            self.final_transition_step(e, script, go_to_next_eff)

    def transition_step(self, p: PlayerState, get_next, can_transition, rest_transition=True):
        return main_transition(self.data, p, lambda player: self.apply_next(player, get_next), can_transition, set(), rest_transition)

    def opponent_transition_step(self, p, script, script_opponent):
        if script is None:
            return script, script_opponent
        return execute_opponent_transition(self.data, p, script, script_opponent)

    def physical_correction_step(self, p1: PlayerState, p2: PlayerState, script_p1: Script, script_p2: Script, old_side: bool):
        if script_p1 is None:
            script_p1 = self.data.get_current_script(p1)
        if script_p2 is None:
            script_p2 = self.data.get_current_script(p2)
        compute_physical_box(p1, script_p1)
        compute_physical_box(p2, script_p2)

        apply_physical_collision(p1, p2, script_p1, script_p2, old_side)

    def position_change_step(self, p: PlayerState, script: Script, opponent_pos_x: FLT, opponent_pos_y: FLT, auto_correct_side: bool) -> Optional[AutoCancel]:
        if script is None:
            return None
        if p.stun is not None:
            if p.pos.ref[1] < 0:
                p.pos.coord[1] -= p.pos.ref[1]
                p.pos.ref[1] = ZERO
        return compute_movement(p, script, opponent_pos_x, opponent_pos_y, auto_correct_side)

    def compute_hit_hurt_step(self, p: PlayerState, script: Script):
        if script is None and p.stun is not None and p.stun > 0:
            return
        script = self.data.get_current_script(p)
        time = p.script.time
        x_shift = script.get_position_shift_at(p.script_state, PositionShiftsConstants.X | PositionShiftsConstants.HITBOX_SHIFT, time) or ZERO
        y_shift = script.get_position_shift_at(p.script_state, PositionShiftsConstants.Y | PositionShiftsConstants.HITBOX_SHIFT, time) or ZERO
        x, y = p.pos.coord
        side = p.pos.side
        y += y_shift
        if side:
            x += x_shift
        else:
            x -= x_shift
        p.hurt = script.get_hurtboxes_at(p.script.time, p.script_state,x, y, side)
        hitboxes = script.get_hitboxes_at(p.script.time, p.script_state, x, y, side)
        if hitboxes:
            hitboxes = filter_hitboxes(p, hitboxes)
        p.hit = hitboxes

    @staticmethod
    def hit_collision(p: PlayerState, script: Script):
        if script is None and p.stun is not None and p.stun > 0:
            return
        perform_hit_collision(p)

    def apply_hit_queue(self, p: PlayerState, script: Script):
        if script is None and p.stun is not None and p.stun > 0:
            return
        if p.hit_queue:
            apply_hit_queue(self.data, p, *p.hit_queue.pop(0))

    @staticmethod
    def final_transition_step(p: PlayerState, script: Script, go_to_next: Optional[AutoCancel]):
        # If we got hit we dont allow go_to_next
        if p.script.time is None:
            go_to_next = None
            p.script.time = 0
        if script is None:
            return
        final_transition(p, script, go_to_next)

    def can_cancel(self, p: PlayerState, move_name: str, move_description: Optional[Move]):
        if move_description is None:
            # Allow FORWARD and BACKWARD as a resting position for cancels
            if is_resting_script(p.char, p.script.script) or is_walking_script(p.char, p.script.script):
                return
            raise MoveNotAllowed(self.data, p, move_name)

        # We check that we could have canceled on the previous frame
        cancels = self.data.get_cancels(p.current_char(), p.script_state, self.data.get_current_script(p), p.script.time)
        if move_description.index not in cancels:
            raise MoveNotAllowed(self.data, p, move_name)
        if move_description.vtrigger_activated > 0 and not p.vtrigger:
            raise MoveNotAllowed(self.data, p, move_name, vtrigger=move_description.vtrigger_consumes)
        if move_description.stance == 1 and p.stance != 0 or move_description.stance == 2 and p.stance != 1:
            raise MoveNotAllowed(self.data, p, move_name, stance=move_description.stance)
        restriction = move_description.position_restriction
        distance = move_description.restriction_distance
        if restriction > 0 and p.pos.coord[1] < distance:
            raise MoveNotAllowed(self.data, p, move_name, p.pos.coord[1], distance)
        # if move_description.is_wall_jump():
        #     if abs(p.pos.coord[0]) != BOUND:
        #         raise MoveNotAllowed(self.data, p, move_name, abs(p.pos.coord[0]), BOUND)
            # we have to override side, so forcing state
            # p.pos.side = p.pos.coord[0] < 0

    def check_and_consume_resources(self, p: PlayerState, move_name: str, move_description: Optional[Move]):
        self.can_cancel(p, move_name, move_description)
        if move_description is None:
            p.move_id = None
            return
        if move_description.vtrigger_consumes > 0 and p.vtrigger > 0:
            p.vtrigger -= move_description.vtrigger_consumes
            if p.vtrigger < 0:
                p.vtrigger = 0
        if move_description.category.bit_length() == 8:
            # vtrigger activation
            p.vtrigger = self.data.chars[p.char].bch.vtrigger
        p.move_id = move_description.index

    def execute(self, p1, p2, move_name: Union[str, List[str]], frames=None, func=None, move_name_p2=None):
        """
        Old execution stack, still used by tests due to it's simple queuing mechanism

        :param p1: the first player
        :param p2: the second player
        :param move_name: the move to apply to p1 (either str or list of script)
        :param frames: the number of frames to execute
        :param func: the function to apply after each frame (takes p1 and p2 as arguments)
        :param move_name_p2: the move to apply to p2 (rest by default)
        """
        self.select_scripts(move_name, p1)
        if move_name_p2 is not None:
            self.select_scripts(move_name_p2, p2)
        p2.recovery = 0
        do_interrupt = frames is None
        if do_interrupt:
            # Don't interrupt until end condition has not been reached
            frames = 100000
        for i in range(frames):
            self.step(p1, p2, frame_count=i)
            position_logger.debug(
                "[%.06f, %.06f] %s[%d] [%.06f, %.06f] %s[%d] [%d] adv: %d",
                p1.pos.coord[0], p1.pos.coord[1], p1.script, p1.stun if p1.stun is not None else -1, p2.pos.coord[0], p2.pos.coord[1], p2.script, p2.stun if p2.stun is not None else -1,
                p2.stun_force.t if p2.stun_force is not None else 0, p2.recovery
            )
            if func is not None:
                func(p1, p2)

            is_p1_resting = is_resting_script(p1.char, p1.script.script) and not p1.script.init
            is_p2_resting = is_resting_script(p2.char, p2.script.script) and not p2.script.init
            if not is_p1_resting and not is_p2_resting:
                p2.recovery = 0
            if is_p1_resting:
                p2.recovery += 1
            if is_p2_resting:
                p2.recovery -= 1
            if do_interrupt:
                if is_p1_resting and is_p2_resting:
                    break

    def select_scripts(self, move_name: Union[str, List[int]], p: PlayerState):
        """
        Select scripts for a player, this will check if the transition is possible and potentially consume resources

        :param move_name: the move to apply
        :param p: the player state
        """
        others = []
        if type(move_name) is list:
            others = move_name[1:]
            move_name = move_name[0]
        scripts, move_description = self.data.get_scripts_by_move_name(p.current_char(), move_name)
        self.check_and_consume_resources(p, move_name, move_description)
        transition_logger.info(
            "[%s][%s] Selecting new move %s [script_id: %d]",
            p.char, p.script, move_name, scripts[0]
        )
        p.select_scripts(
            scripts + [s for m in others for s in self.data.get_scripts_by_move_name(p.current_char(), m)[0]])

    def apply_next(self, p: PlayerState, get_next):
        """
        Apply the next move given by get_next to the player state, will be given to transition_step
        Will check if the move is legal or not

        :param p: the player state
        :param get_next: the function that returns (move, hold)
        """
        move_name, hold = get_next(p)
        if type(move_name) is list:
            script = move_name
        else:
            script, move_description = self.data.get_scripts_by_move_name(p.char, move_name)
            self.check_and_consume_resources(p, move_name, move_description)
        p.select_scripts(script)
        p.hold = hold
