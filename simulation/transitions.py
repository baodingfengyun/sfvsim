import random
from functools import reduce
from sfvfile import Move, AutoCancel, Script

from typing import Optional, Set, Tuple
from model import ZERO, FLT, PlayerState, Position, SFVData, is_resting_script, is_walking_script, \
    PositionShiftsConstants, reserved_cancel_list, is_landing_required, is_on_ground, get_script_length, is_bound, \
    StatusFlag, is_falling, is_knockdown
from .log import position_logger, transition_logger


class InfiniteTransitionException(Exception):
    pass


def main_transition(data: SFVData, p: PlayerState, apply_next, can_transition, auto_cancel_used: Set[AutoCancel],
                    rest_transition: bool = True, recursion_count=0) -> Optional[Script]:
    """
    Transition step: first step of the process, it will select the right script to execute

    :param data: the sfv data
    :param p: the player
    :param apply_next: function called when the next move needs to be queued
    :param can_transition: function that returns true when a move is available in apply_next
    :param rest_transition: if False the transition step cannot transition to a resting state (avoid infinite recursion)
    :return: the script that will be used or None if no script will be executed this frame (stun for instance)
    """
    if recursion_count > 10:
        raise InfiniteTransitionException("Internal Error: Exceeded recursion limit in %s" % str(p.script))

    if p.stun is not None:
        if p.stun <= 0:
            p.stun = None
        else:
            p.stun -= 1
            if not p.script.init:
                return None

    # Because it is too hard to sync with actual data
    # Every rest position stays at 0
    resting_script = is_resting_script(p.char, p.script.script) if p.effects is not None else False
    if resting_script:
        if p.stun_force is not None:
            p.stun_force = None
        p.opponent.combo_counter = 0
        if p.effects is not None:
            p.script.time = 0
            p.script.init = True
            # If we stand we can turn either way
            p.pos.side = p.pos.coord[0] < p.opponent.pos.coord[0]
        # Reset juggle state
        p.juggle_state = "NONE"
        p.juggle = 0

    script = data.get_current_script(p)
    if not p.script.init or resting_script or is_walking_script(p.char, p.script.script):
        cancel_lists = script.get_cancels_at(p.script.time, p.script_state)
        cancel_buffer = {
            cancel.cancel_list: cancel.type
            for cancel in cancel_lists
            if cancel.cancel_list >= 0 and cancel.type < 8
        }
        for rcl in reserved_cancel_list(p.char, script.index):
            cancel_buffer[rcl] = 0
        if cancel_buffer:
            # update buffered cancels
            for cl, ctype in cancel_buffer.items():
                if can_transition(cl):
                    p.buffered_cancels[cl] = ctype
        if p.buffered_cancels:
            # try to launch
            hit_status = p.hit_status(script.last_action_frame)
            cancels = {cancel.cancel_list for cancel in cancel_lists if
                      cancel.cancel_list >= 0 and cancel.type == 8} | reserved_cancel_list(p.char, script.index)
            valid_cancel = False
            for cl, ctype in p.buffered_cancels.items():
                if cl not in cancels:
                    continue
                if ctype == 0 or (ctype & hit_status) or (ctype & 4 and hit_status == 0):
                    valid_cancel = True
                    break
            if valid_cancel:
                apply_next(p)
                return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)

    if p.effects is None and p.script.time >= 0:
        # If this projectile has no more hitboxes, do a transition
        all_hitboxes_tick_end = [h.tick_start for h in script.hitboxes if h.hit_type != 4]
        latest_hitbox = max(all_hitboxes_tick_end) if all_hitboxes_tick_end else None
        if latest_hitbox is not None and latest_hitbox < p.script.time and not p.hit:
            transition_logger.info("[%s][%s] Effect has no more hitbox", p.char, p.script)
            if can_transition(None):
                apply_next(p)
                return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)

    if resting_script:
        p.move_id = None
        if rest_transition:
            if can_transition(None):
                apply_next(p)
                # Avoid infinite recursion
                return main_transition(data, p, apply_next, can_transition, auto_cancel_used, rest_transition=False, recursion_count=recursion_count+1)

    # TODO Relax this condition when #25 is fixed
    total_ticks = script.total_ticks if script.interrupt_frame < 0 or (script.interrupt_frame == 0 and script.index < 30) else min(script.total_ticks,
                                                                            script.interrupt_frame + 1)

    force_finish = False
    if script.interrupt_frame > 0 and not is_landing_required(script.flags):
        if p.script.time == script.interrupt_frame:
            transition_logger.info("[%s][%s] Forcing script to interrupt", p.char, p.script)
            force_finish = True

    p.script.previous_time = p.script.time
    if p.script.init:
        if script.execution_flags & 1:
            p.reset_shift()
        p.pos.force[0] *= FLT(script.slide_ax)
        p.pos.force[1] *= FLT(script.slide_ay)
        p.pos.speed[0] *= FLT(script.slide_vx)
        p.pos.speed[1] *= FLT(script.slide_vy)
        if is_on_ground(script.flags) and p.effects is not None:
            p.pos.ref[1] = ZERO
            p.juggle_state = "NONE"
            p.juggle = 0
        p.script.init = False
    else:
        if p.script.speed:
            p.script.speed[2] -= 1
            new_tick = p.script.tick + p.script.speed[0]
            if p.script.speed[2] < p.script.speed[1]:
                new_tick += 1
            p.script.tick = new_tick % 65536
            p.script.time += new_tick // 65536
            if p.script.speed[2] == 0:
                p.script.speed = None
                p.script.tick = 0
                if p.script.time >= get_script_length(script):
                    p.script.time = get_script_length(script) - 1
        else:
            p.script.tick = 0
            p.script.time += 1

    if is_bound(script.flags):
        p.script.countdown = None
        if p.script.time == 0:
            if p.stun_force is not None:
                # dirty, should not be done here I think
                if script.slide_vx > 0.0:
                    p.stun_force.speed[0] *= FLT(script.slide_vx * 2)
                    p.stun_force.force[0] = FLT(-p.stun_force.speed[0] / script.total_ticks)
                    p.pos.force[1] = p.pos.speed[1] = ZERO
                    p.stun_force.t = script.total_ticks
                else:
                    p.stun_force = None

        # Bound cancel on 2nd frame
        if p.script.time == 1 and 220 <= p.script.script < 230:
            offset_d = 1 if p.script.script >= 223 else 0
            if p.rise == "NONE" or p.knockdown_type == "HARD":
                p.queue = [235 + offset_d, 240 + offset_d]
            else:
                p.pos.side = p.pos.coord[0] < p.opponent.pos.coord[0]
                p.stun_force = None
                p.select_scripts \
                    ([246 + offset_d if p.rise == "QUICK" or p.knockdown_type == "STAND" else 244 + offset_d])
                return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)

    if p.script.countdown == 0:
        transition_logger.info("[%s][%s] Forcing script finish due to countdown", p.char, p.script)
        force_finish = True
        p.script.countdown = None
    if p.auto_cancel and p.auto_cancel.script_index < 0:
        transition_logger.info("[%s][%s] Forcing script to finish due to auto cancel %d", p.char, p.script, p.auto_cancel.condition)
        p.auto_cancel = None
        force_finish = True

    if p.script.time >= total_ticks or force_finish:
        p.script.tick = 0
        if not force_finish:
            auto_cancel_land = [b.tick_start for b in script.get_auto_cancels_at(p.script.time - 1, p.script_state) if b.condition == 8]
            falling = is_falling(script.flags) or auto_cancel_land
            falling = falling and not [s for s in script.get_status_at(p.script.time - 1, p.script_state) if s.player_flags & StatusFlag.ON_GROUND]
            if p.script.countdown is not None:
                if p.script.countdown > 0:
                    transition_logger.debug("[%s][%s] Looping script to t=0 due to countdown", p.char, p.script)
                    p.script.time = 0
                    return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)
            elif falling and (p.script.temp_char is None or p.lock_transition):
                transition_logger.debug("[%s][%s] Looping to t=original_position due to falling", p.char, p.script)
                p.script.time = total_ticks - 2
                p.disable_positions = True
                return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)

        if p.script.time < script.total_ticks:
            # Dirty but all my other way are buggy
            # #22 if we get out of a script by interruption, apply the Always auto cancel if available
            auto_cancel_always = [b for b in script.get_auto_cancels_at(script.total_ticks - 1, p.script_state) if b.condition == 0]
            if len(auto_cancel_always):
                transition_logger.debug("[%s][%s] Placing script %s in queue because of AutoCancel on interrupt",
                                        p.char, p.script, auto_cancel_always[0].script_index)
                p.queue = [auto_cancel_always[0].script_index] + p.queue
        if is_bound(script.flags):
            p.stun_force = None
        if is_knockdown(script.flags):
            p.queue = [220 + script.type_offset]
        if p.queue:
            if p.queue[0] >= 400:
                # Mostly useless because we should not have move scripts in queue
                # Should be a rest transition
                p.pos.side = p.pos.coord[0] < p.opponent.pos.coord[0]
            transition_logger.info("[%s][%s] Transitioning to next queued script %s",
                                   p.char, p.script, p.queue[0])
            p.select_scripts(p.queue)
            p.script.time = 0
            if p.script.script == 235 or p.script.script == 236:
                p.script.countdown = p.downtime
            return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)
        # Dont interrupt if we have nothing better to do!
        transition_logger.info("[%s][%s] Transitioning to rest position", p.char, p.script)
        if p.effects is None:
            if can_transition(None):
                apply_next(p)
            else:
                return None
        else:
            p.select_scripts([0])
        return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)

    if p.auto_cancel:
        go_to_next = p.auto_cancel
        p.auto_cancel = None
    else:
        go_to_next = get_auto_cancel_transition(p, script, auto_cancel_used)

    if go_to_next is not None:
        auto_cancel_used.add(go_to_next)
        recursion = apply_auto_cancel(p, go_to_next)
        if recursion:
            if p.script.speed:
                # recompute script speed
                current_script = data.get_current_script(p)
                recovery = (p.stun_force.t - 1)
                speed = int(65536 * (get_script_length(current_script) - go_to_next.script_time) / recovery)
                p.script.speed = [speed, recovery - (65536 - speed*recovery % 65536) % 65536, recovery]
            return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)
        if p.script.time >= total_ticks:
            p.script.time = total_ticks - 1

    apply_transition_step_others(p, script)

    p.hold -= 1

    return script


def apply_auto_cancel(p: PlayerState, go_to_next: AutoCancel):
    if go_to_next.state_change & 0x80:
        p.script_state |= go_to_next.execution_flag
        return False
    if go_to_next.script_index < 0:
        p.script.time = go_to_next.script_time
        transition_logger.info("[%s][%s] AutoCancel condition=%s to itself, placing t=%d", p.char, p.script,
                               go_to_next.condition, p.script.time)
        return False
    if go_to_next.execution_flag & 2:
        # continue seamlessly
        p.change_script(go_to_next.script_index, p.script.time)
        transition_logger.info("[%s][%s] AutoCancel condition=%s to script %d without changing time",
                               p.char, p.script, go_to_next.condition, go_to_next.script_index)
        return p.script.init
    desired_time = go_to_next.script_time if go_to_next.script_index >= 0 and go_to_next.script_time >= 0 else 0
    transition_logger.info("[%s][%s] AutoCancel condition=%s to script %d at time %d",
                           p.char, p.script, go_to_next.condition, go_to_next.script_index, desired_time)
    p.change_script(go_to_next.script_index, desired_time)
    return p.script.init


def apply_transition_step_others(p: PlayerState, script: Script):
    commands = script.get_commands_at(p.script.time, p.script_state)
    if commands.size() == 0:
        return

    other = commands.find_one(0, 9)
    if other:
        if p.script.time == 0 and other.parameters[0] <= 0:
            position_logger.info("[%s][%s] Resetting position due to 'other' = (0,9)", p.char, p.script)
            p.pos = Position(0, 0)

    other = commands.find_one(0, 1)
    if other:
        # stance change
        p.stance = other.parameters[0]

    others = commands.find_all(0, 3)
    for other in others:
        # spawn projectile
        script_id = other.parameters[0]
        transition_logger.info("[%s][%s] Spawning projectile %d", p.char, p.script, script_id)
        side = FLT(1 if p.pos.side else -1)
        effect = PlayerState(p.char, p.pos.ref[0] + side * FLT(other.parameters[1] / 100),
                             p.pos.ref[1] + FLT(other.parameters[2] / 100))
        effect.move_id = p.move_id
        # Disable effects
        effect.effects = None
        effect.opponent = p.opponent
        effect.select_scripts([script_id])
        p.opponent.opponent.effects.append(effect)
        effect.pos.side = p.pos.side
        # important: share the hitstate reference in order to transmit cancels properties to the player
        effect.hit_state = p.hit_state
        effect.script.init = True


def get_auto_cancel_transition(p: PlayerState, script: Script, used) -> Optional[AutoCancel]:
    # Resolve auto cancels
    # It should not need p.script.time - 1... feels like a SFV BUG
    # but that's the only way I support z21-gnd-tatsu-lk.json
    auto_cancels = script.get_auto_cancels_at(p.script.time - 1, p.script_state) + script.get_auto_cancels_at(p.script.time, p.script_state)
    if not auto_cancels:
        return
    hold_transition = []
    all_hitboxes = set(p.hit_state.hit_hitboxes)
    has_hit = p.hit_state.has_hit > 0
    for ac in auto_cancels:
        if ac in used:
            continue
        # We had one collision
        # Test the hit_transitions
        if all_hitboxes:
            if ac.condition == 33:
                return ac
            base_condition = 1 if has_hit else 2
            if ac.condition == base_condition:
                return ac
            if (ac.condition == 4 and has_hit) or (ac.condition == 34):
                hitbox = 0 if not len(ac.parameters) else ac.parameters[0]
                mode = 0 if not len(ac.parameters) else ac.parameters[3]
                if mode == 0:
                    can_transition = hitbox in all_hitboxes
                else:
                    can_transition = len(all_hitboxes) >= hitbox
                if can_transition:
                    return ac
        if not p.hit_state.hit_hitboxes and ac.condition == 3:
            # we have a condition on hitboxes, yet we have none that has hit
            return ac
        # Corner transition
        if ac.condition == 9 and (p.pos.coord[0] >= (7.5 - (ac.parameters[0] if len(ac.parameters) > 0 else 0)) or p.pos.coord[0] <= -(7.5 - (ac.parameters[0] if len(ac.parameters) > 0 else 0))):
            return ac
        # Proximity transition
        if ac.condition == 12:
            if ac.parameters[0] & 1:
                # distance condition
                if abs(p.pos.coord[0] - p.opponent.pos.coord[0]) < (0.25 + ac.parameters[1] / 100):
                    # BUGSFV?
                    # CMY V_SKILL close enough skips one frame for some reason
                    if abs(p.pos.coord[0] - p.opponent.pos.coord[0]) > (ac.parameters[1] / 100):
                        p.script.time -= 1
                    return ac
            else:
                if abs(p.pos.coord[0] - p.opponent.pos.coord[0]) > (0.25 + ac.parameters[1] / 100):
                    return ac
        # Projectile presence transition
        if ac.condition == 26 and p.effects:
            return ac
        # Resource presence transition
        if ac.condition == 20:
            # V-Trigger
            if ac.parameters[1] == 2 and p.vtrigger > 0:
                return ac
            # params[1] == 1 -> no more kunais (let's forget this one for now)
            continue
        # Button hold
        if ac.condition == 24 and p.hold > 0:
            # accumulate than randomize
            hold_transition.append(ac)
        if ac.condition == 14:
            # params is [condition, buttons, ?, ?]
            # hold all buttons
            if ac.parameters[0] == 0 and p.hold > 0:
                return ac
            # interrupt with buttons (BRD CHAIN)
            if ac.parameters[0] == 1:
                # not supported
                continue
            # release any or release both
            if ac.parameters[0] == 2 or ac.parameters[0] == 3:
                if p.hold <= 0:
                    return ac
                continue
            # hold any button
            if ac.parameters[0] == 4 and p.hold > 0:
                return ac

        if ac.condition in {16, 17} and p.armor:
            sub_condition_mask = ac.parameters[0] if len(ac.parameters) else 0
            if sub_condition_mask:
                all_hitboxes = [(h.hit_level << 1) or 1 for armored in p.armor.values() for h in armored]
                hit_mask = reduce(lambda a, b: (a | b), all_hitboxes, 0)
                # we now have hit mask with: 1 = mid, 2 = high, 4 = low
                # unfortunately, condition mask is 1 = low, 2 = mid, 4 = high
                # here's the alignment
                hit_mask = ((hit_mask << 1) | (hit_mask >> 2)) & 7
                if hit_mask & sub_condition_mask:
                    return ac
            else:
                return ac

        if ac.condition == 0 and ac.tick_start == p.script.time:
            return ac

    if hold_transition:
        return hold_transition[random.randrange(0, len(hold_transition))]

    return None


def get_auto_cancel_transition_post_hit(p: PlayerState, script: Script) -> Optional[AutoCancel]:
    auto_cancels = script.get_auto_cancels_at(p.script.time, p.script_state)
    if not auto_cancels:
        return
    for ac in auto_cancels:
        if ac.condition == 5:
            all_hitboxes = set(p.hit_state.hit_hitboxes)
            for eff in p.effects or []:
                all_hitboxes.update(eff.hit_state.hit_hitboxes)
            hitbox = 0 if not len(ac.parameters) else ac.parameters[0]
            mode = 0 if not len(ac.parameters) else ac.parameters[3]
            if mode == 0:
                can_transition = hitbox in all_hitboxes
            else:
                can_transition = len(all_hitboxes) >= hitbox
            if can_transition:
                return ac
        if ac.condition == 15 and p.armor:
            return ac


def execute_opponent_transition(data: SFVData, p: PlayerState, script: Script,
                                script_opponent: Script) -> Tuple[Script, Script]:
    # select the opponent move pre-transition
    opponent_move = data.get_current_move_description(p.opponent)
    # Separated because it depends on the transition step
    others = script.get_commands_at(p.script.time, p.script_state)
    if others.contains(0, 4):
        # Tech throw
        if opponent_move is not None and (opponent_move.subcategories & 0x20 and opponent_move.category == 0):
            transition_logger.info("[%s][%s] Throw tech! Opponent move %s", p.char, p.script, opponent_move.name)
            # the opponent is/was doing a throw
            # p is the attacker
            p.select_scripts([40])
            # p.opponent is the defender
            p.opponent.select_scripts([41])
            script_opponent = data.get_current_script(p.opponent)
            p.pos = Position(p.opponent.pos.ref[0], p.opponent.pos.ref[1])
            p.opponent.script.init = False
            p.script.init = False
            # avoid going into (0,2)
            return data.get_current_script(p), script_opponent
        # TODO: Post hit throw tech, needs special cancel or input based interruption (like done in the game)

    op_transition = others.find_one(0, 2)
    if op_transition:
        # Throws, Command Grabs, some CA
        # Opponent transition into character script
        # Params[0] = 1 -> Start
        # Params[0] = 0 -> End of the script
        script_id = op_transition.parameters[1]
        if op_transition.parameters[0]:
            transition_logger.info("[%s][%s] Transitioning opponent to script %d", p.char, p.script, script_id)
            p.opponent.select_scripts([script_id])
            p.opponent.script.time = op_transition.parameters[2]
            p.opponent.script.temp_char = p.char
            p.opponent.script.init = False
            p.opponent.pos = Position(p.pos.ref[0], p.pos.ref[1])
            p.opponent.pos.side = p.pos.side
            p.opponent.stun = None
            p.opponent.stun_force = None
            p.opponent.juggle_state = "NONE"
            p.opponent.juggle = 0
            p.opponent.lock_transition = True
        else:
            transition_logger.info("[%s][%s] Disabling transition locking %d", p.char, p.script, script_id)
            p.opponent.lock_transition = False
        script_opponent = data.get_current_script(p.opponent)

    return script, script_opponent


def final_transition(p: PlayerState, script: Script, go_to_next: Optional[AutoCancel]):
    """
    Transition step executed at the end of the frame

    :param p: the player state
    :param script: the script
    :param go_to_next: the optional auto cancel coming from earlier steps (landing for instance)
    """
    if p.script.countdown is not None:
        p.script.countdown -= 1

    # decrease vtrigger only if both are not in stun
    if p.stun is None and p.opponent.stun is None and p.vtrigger > 0:
        p.vtrigger -= 1

    if p.script.init:
        # We have changed script, therefore we cannot use the current script
        return

    auto_cancel_post_hit = get_auto_cancel_transition_post_hit(p, script)

    if auto_cancel_post_hit:
        go_to_next = auto_cancel_post_hit

    # apply time freeze (meaning stun for the opponent)
    others = script.get_commands_at(p.script.time, p.script_state)
    time_freeze = others.find_one(1, 0)
    if time_freeze:
        freeze_frames = time_freeze.parameters[1] + 2
        p.opponent.stun = (p.opponent.stun or 0) + freeze_frames
        for e in p.opponent.effects + p.effects:
            e.stun = (e.stun or 0) + freeze_frames
    p.auto_cancel = go_to_next
